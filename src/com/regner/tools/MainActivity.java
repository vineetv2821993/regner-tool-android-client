 package com.regner.tools;


import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;

public class MainActivity extends Activity {
	public final String COMMAND_TOOL = "c:\\command\\nircmd ";
	public static ImageButton  bScreen, bShutdown, bLogoff, bCD, bBright, bAudio,bBrightD,bHibernate,bResolution,bSleep;
	public static boolean  bScreenToggle, bShutdownToggle, bLogoffToggle, bCDToggle, bBrightToggle, bAudioToggle, bBrightDToggle,bHibernateToggle,bResolutionToggle,bSleepToggle;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		//Toast.makeText(getBaseContext(), ClientThread.SERVER_IP, Toast.LENGTH_LONG).show();
		bScreen  = (ImageButton) findViewById(R.id.imageButtonScreen);
	    bScreenToggle = true;
		bScreen.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub	
				
				Intent NextActivity = new Intent("com.regner.tools.TouchScreenActivity");
				startActivity(NextActivity);
				
				if(bScreenToggle == true){
				bScreen.setImageResource(R.drawable.b1);
				bScreenToggle = false;
				}
				else{
					bScreen.setImageResource(R.drawable.button1);
					bScreenToggle = true;
				}
			}
		});		
		
		bAudio  = (ImageButton) findViewById(R.id.imageButtonAudio);
	    bAudioToggle = true;
		bAudio.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				
				
				if(bAudioToggle == true){
				bAudio.setImageResource(R.drawable.b8a);
				bAudioToggle = false;
				ClientThread.setCommand(COMMAND_TOOL+"mutesysvolume 1");
				ClientThread.myThread = new Thread(new ClientThread());
				ClientThread.myThread.start();
				}
				else{
					bAudio.setImageResource(R.drawable.button8a);
					bAudioToggle = true;
					ClientThread.setCommand(COMMAND_TOOL+"mutesysvolume 0");
					ClientThread.myThread = new Thread(new ClientThread());
					ClientThread.myThread.start();	
				}
			}
		});		
		bBright  = (ImageButton) findViewById(R.id.imageButtonBright);
	    bBrightToggle = true;
		bBright.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				
				
				if(bBrightToggle == true){
				bBright.setImageResource(R.drawable.b7a);
				bBrightToggle = false;
				
				ClientThread.setCommand(COMMAND_TOOL+"setbrightness 100");
				ClientThread.myThread = new Thread(new ClientThread());
				ClientThread.myThread.start();
				}
				else{
					bBright.setImageResource(R.drawable.button7a);
					bBrightToggle = true;
					
					ClientThread.setCommand(COMMAND_TOOL+"setbrightness 75");
					ClientThread.myThread = new Thread(new ClientThread());
					ClientThread.myThread.start();
				}
			}
		});		
		bBrightD  = (ImageButton) findViewById(R.id.imageButtonBrightD);
	    bBrightDToggle = true;
		bBrightD.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				
				
				if(bBrightDToggle == true){
				bBrightD.setImageResource(R.drawable.bright);
				bBrightDToggle = false;
				
				ClientThread.setCommand(COMMAND_TOOL+"setbrightness 50");
				ClientThread.myThread = new Thread(new ClientThread());
				ClientThread.myThread.start();
				}
				else{
					bBright.setImageResource(R.drawable.brightlow);
					bBrightToggle = true;
					
					ClientThread.setCommand(COMMAND_TOOL+"setbrightness 0");
					ClientThread.myThread = new Thread(new ClientThread());
					ClientThread.myThread.start();
				}
			}
		});
		bCD  = (ImageButton) findViewById(R.id.imageButtonCD);
	    bCDToggle = true;
		bCD.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub



				
				if(bCDToggle == true){
				bCD.setImageResource(R.drawable.b2);
				bCDToggle = false;
				ClientThread.setCommand(COMMAND_TOOL+"cdrom open");
				ClientThread.myThread = new Thread(new ClientThread());
				ClientThread.myThread.start();	
				}
				else{
					bCD.setImageResource(R.drawable.button2);
					bCDToggle = true;
					ClientThread.setCommand(COMMAND_TOOL+"cdrom close");
					ClientThread.myThread = new Thread(new ClientThread());
					ClientThread.myThread.start();	
				}
			}
		});		
		
		bLogoff  = (ImageButton) findViewById(R.id.imageButtonLogoff);
	    bLogoffToggle = true;
		bLogoff.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				/*
	
				*/
				if(bLogoffToggle == true){
				bLogoff.setImageResource(R.drawable.b13);
				bLogoffToggle = false;
				ClientThread.setCommand(COMMAND_TOOL+"exitwin logoff");
				ClientThread.myThread = new Thread(new ClientThread());
				ClientThread.myThread.start();
				}
				else{
					bLogoff.setImageResource(R.drawable.button13);
					bLogoffToggle = true;
					ClientThread.setCommand(COMMAND_TOOL+"exitwin logoff");
					ClientThread.myThread = new Thread(new ClientThread());
					ClientThread.myThread.start();
				}
			}
		});		

		
		bShutdown  = (ImageButton) findViewById(R.id.imageButtonShutdown);
	    bShutdownToggle = true;
		bShutdown.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				/*

				*/
				if(bShutdownToggle == true){
				bShutdown.setImageResource(R.drawable.b12);
				bShutdownToggle = false;
				ClientThread.setCommand(COMMAND_TOOL+"exitwin shutdown");
				ClientThread.myThread = new Thread(new ClientThread());
				ClientThread.myThread.start();	
				}
				else{
					bShutdown.setImageResource(R.drawable.button12);
					bShutdownToggle = true;
					ClientThread.setCommand(COMMAND_TOOL+"exitwin shutdownforce");
					ClientThread.myThread = new Thread(new ClientThread());
					ClientThread.myThread.start();	
				}
			}
		});		
		
		bSleep  = (ImageButton) findViewById(R.id.imageButtonSleep);
	    bSleepToggle = true;
		bSleep.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				/*

				*/
				if(bSleepToggle == true){
				bSleep.setImageResource(R.drawable.sleep);
				bSleepToggle = false;
				ClientThread.setCommand(COMMAND_TOOL+"monitor on");
				ClientThread.myThread = new Thread(new ClientThread());
				ClientThread.myThread.start();	
				}
				else{
					bSleep.setImageResource(R.drawable.sleepglow);
					bSleepToggle = true;
					ClientThread.setCommand(COMMAND_TOOL+"monitor off");
					ClientThread.myThread = new Thread(new ClientThread());
					ClientThread.myThread.start();
				}
			}
		});		
		
		bHibernate  = (ImageButton) findViewById(R.id.imageButtonHibernate);
	    bHibernateToggle = true;
		bHibernate.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				/*
				ClientThread.setCommand(COMMAND_TOOL+"sendkeypress ctrl+p");
				ClientThread.myThread = new Thread(new ClientThread());
				ClientThread.myThread.start();	
				*/
				if(bHibernateToggle == true){
				bHibernate.setImageResource(R.drawable.hibernate);
				bHibernateToggle = false;
				ClientThread.setCommand(COMMAND_TOOL+"hibernate");
				ClientThread.myThread = new Thread(new ClientThread());
				ClientThread.myThread.start();
				}
				else{
					bHibernate.setImageResource(R.drawable.hibernateglow);
					bHibernateToggle = true;
					ClientThread.setCommand(COMMAND_TOOL+"hibernate force");
					ClientThread.myThread = new Thread(new ClientThread());
					ClientThread.myThread.start();
				}
			}
		});		

		bResolution  = (ImageButton) findViewById(R.id.imageButtonResolution);
	    bResolutionToggle = true;
		bResolution.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				/*

				*/
				if(bResolutionToggle == true){
				bResolution.setImageResource(R.drawable.resolution);
				bResolutionToggle = false;
				ClientThread.setCommand(COMMAND_TOOL+"setdisplay 1024 768 32");
				ClientThread.myThread = new Thread(new ClientThread());
				ClientThread.myThread.start();	
				
				}
				else{
					bResolution.setImageResource(R.drawable.resolutionglow);
					bResolutionToggle = true;
					ClientThread.setCommand(COMMAND_TOOL+"setdisplay 1366 768 32");
					ClientThread.myThread = new Thread(new ClientThread());
					ClientThread.myThread.start();	
				}
			}
		});		

	}
 
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
